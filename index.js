const { error } = require("console");
const { got } = require("./data-1.js");
const fs = require('fs');

function countAllPeople(object) {
    let peopleCount = 0;
    object.houses.forEach((house) => {
        peopleCount += house.people.length;
    });

    // Using reduce
    // const peopleCount = object.houses.reduce((count, house) => {
    //     count += house.people.length;
    //     return count
    // }, 0)
    return peopleCount;
}


function peopleByHouses(object) {
    const peopleByHouse = object.houses.reduce((people, house) => {
        people[house.name] = house.people.length;
        return people;
    }, {})
    return peopleByHouse;
}


function everyone(object) {
    return object.houses.reduce((names, house) => {
        house.people.forEach((people) => {
            names.push(people.name);
        });
        return names;
    }, [])
}


function nameWithS(object) {
    return object.houses.reduce((namesWithS, house) => {
        house.people.forEach((people) => {
            if (people.name[0].toLowerCase() == 's') {
                namesWithS.push(people.name);
            }

        });
        return namesWithS;
    }, [])
}


function nameWithA(object) {
    return object.houses.reduce((namesWithA, house) => {
        house.people.forEach((people) => {
            if (people.name[0].toLowerCase() == 'a') {
                namesWithA.push(people.name);
            }

        });
        return namesWithA;
    }, []);
}


function surnameWithS(object) {
    return object.houses.reduce((surnamesWithA, house) => {
        house.people.forEach((people) => {
            let surname = people.name.split(' ')[1][0];
            if (surname == 'S') {
                surnamesWithA.push(people.name);
            }

        });
        return surnamesWithA;
    }, []);
}


function surnameWithA(object) {
    return object.houses.reduce((surnamesWithA, house) => {
        house.people.forEach((people) => {
            let surname = people.name.split(' ')[1][0];
            if (surname == 'A') {
                surnamesWithA.push(people.name);
            }

        });
        return surnamesWithA;
    }, []);
}




function peopleNameOfAllHouses(object) {
    return object.houses.reduce((peopleNameByHouse, house) => {
        peopleNameByHouse[house.name] = house.people.map((peopleNames => peopleNames.name))
        return peopleNameByHouse;

    }, {})
}

// console.log("Total Number of peoples");
// console.log(countAllPeople(got));
// console.log("\nTotal number of people by house names");
// console.log(peopleByHouses(got));
// console.log("\nNames of every people in houses");
// console.log(everyone(got));
// console.log("\nNames starting with 's' or 'S'");
// console.log(nameWithS(got));
// console.log("\nNames starting with 'a' or 'A'");
// console.log(nameWithA(got));
// console.log("\nSurname starting with 'S'");
// console.log(surnameWithS(got));
// console.log("\nSurname starting with 'A'");
// console.log(surnameWithA(got));
// console.log("\nPeople Name of all houses by their house name");
// console.log(peopleNameOfAllHouses(got));
try {

    let jsonData = {
        "Total Number of peoples": countAllPeople(got),
        "Total number of people by house names": peopleByHouses(got),
        "Names of every people in houses": everyone(got),
        "Names starting with 's' or 'S'": nameWithS(got),
        "Names starting with 'a' or 'A'": nameWithA(got),
        "Surname starting with 'S'": surnameWithS(got),
        "Surname starting with 'A'": surnameWithA(got),
        "People Name of all houses by their house name": peopleNameOfAllHouses(got)
    }
    fs.writeFile(`./output.json`, JSON.stringify(jsonData, null, 2), (err) => {
        if (err) throw err;
    });
} catch (e) {
    console.log(e.message);
}